// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.4 (swiftlang-1103.0.32.9 clang-1103.0.32.53)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name CommonSDK
import Foundation
import Swift
extension Common {
  public struct Error : Swift.Error, Swift.Codable {
    public var errorCode: Swift.String
    public var errorMessage: Swift.String?
    public var action: Swift.Int?
    public init(errorCode: Swift.String, errorMessage: Swift.String, action: Swift.Int = ErrorAction.unexpected.rawValue)
    public init(from decoder: Swift.Decoder) throws
    public func encode(to encoder: Swift.Encoder) throws
  }
  public struct OldApiError : Swift.Error, Swift.Codable {
    public var errorCode: Swift.String?
    public var errorMessage: Swift.String?
    public var action: Swift.Int?
    public init(errorCode: Swift.String, errorMessage: Swift.String, action: Swift.Int = ErrorAction.unexpected.rawValue)
    public init(from decoder: Swift.Decoder) throws
    public func encode(to encoder: Swift.Encoder) throws
  }
}
public enum ValidationType {
  case notEmpty
  case notNil
  case biggerThanZero
  case notEmptyList
  public static func == (a: CommonSDK.ValidationType, b: CommonSDK.ValidationType) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public protocol UseCase {
  associatedtype Repository
  var repository: Self.Repository { get set }
  associatedtype R
  associatedtype C
  func execute(request: Self.R?, completion: ((Self.C) -> Swift.Void)?)
}
extension UseCase {
  public func execute(completion: @escaping (Self.C) -> Swift.Void)
  public func execute()
  public func execute(request: Self.R?)
}
public protocol Repository {
  var dataProvider: CommonSDK.DataProvider { get set }
}
public protocol ValidatableProtocol {
  var isValid: Swift.Bool { get }
  var group: Swift.Int { get }
}
@propertyWrapper public struct Validation<T> : CommonSDK.ValidatableProtocol {
  public var group: Swift.Int
  public var validations: [CommonSDK.ValidationType]
  public init(validations: [CommonSDK.ValidationType], group: Swift.Int = 0)
  public init(wrappedValue: T?)
  public var projectedValue: CommonSDK.Validation<T> {
    get
  }
  public var wrappedValue: T? {
    get
    set
  }
  public var isValid: Swift.Bool {
    get
  }
}
extension Validation : Swift.Decodable where T : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
extension Validation : Swift.Encodable where T : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public enum ErrorAction : Swift.Int, Swift.Codable {
  case unexpected
  case token
  case otp
  case login
  case register
  case forceUpdate
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
  public init?(rawValue: Swift.Int)
}
public protocol NetworkingDefinitionProtocol {
  func initNetwork(_ clientId: Swift.String, _ clientSecret: Swift.String, _ clientNumber: Swift.String, _ channelId: Swift.String, _ channelName: Swift.String, _ deviceId: Swift.String, _ environment: CommonSDK.Common.Environment, _ appVersion: Swift.String?, _ enableLog: Swift.Bool?, completion: @escaping (Swift.String) -> Swift.Void)
  func postData(_ urlKey: Swift.String, _ data: Swift.String?, completion: @escaping (Swift.String) -> ())
  func setAccessToken(accessToken: Swift.String)
  func setNebulaAccessToken(nebulaAccessToken: Swift.String)
  func addHeader(_ headerKey: Swift.String, _ headerValue: Swift.String)
  func removeHeader(_ headerKey: Swift.String)
  func getHeaders() -> [Swift.String : Any]
}
extension Common {
  public enum Environment : Swift.String {
    case DEV
    case TEST
    case UAT
    case STAGE
    case PROD
    public typealias RawValue = Swift.String
    public init?(rawValue: Swift.String)
    public var rawValue: Swift.String {
      get
    }
  }
}
@_hasMissingDesignatedInitializers public class CommonManager {
  public static var networking: CommonSDK.NetworkingDefinitionProtocol?
  @objc deinit
}
extension Encodable {
  public func toJSON() -> Swift.String
}
extension Dictionary {
  public var jsonStringRepresentation: Swift.String? {
    get
  }
}
extension Data {
  public var utf8EncodedString: Swift.String {
    get
  }
}
extension String {
  public var utf8EncodedData: Foundation.Data {
    get
  }
  public var firstLowercased: Swift.String {
    get
  }
}
public struct Common {
}
public struct Metadata {
  public var url: Swift.String
  public var path: Swift.String
  public var body: Swift.String
  public var method: CommonSDK.Metadata.Method
  public var headers: [Swift.String : Swift.String]?
  public init(url: Swift.String = "", body: Swift.String = "", method: CommonSDK.Metadata.Method = .Post, headers: [Swift.String : Swift.String] = [:])
  public enum Method : Swift.String {
    case Post
    case Get
    public typealias RawValue = Swift.String
    public init?(rawValue: Swift.String)
    public var rawValue: Swift.String {
      get
    }
  }
}
public protocol ModelValidator {
  var validate: Swift.String { get }
}
extension ModelValidator {
  public var validate: Swift.String {
    get
  }
}
public struct NetworkingCallBack<T> {
  public var response: T?
  public var error: CommonSDK.Common.Error?
  public init(response: T?, error: CommonSDK.Common.Error?)
}
public protocol DataProvider {
  func execute(metadata: CommonSDK.Metadata, completion: @escaping (CommonSDK.NetworkingCallBack<Swift.String>) -> ())
}
public struct ErrorListResponse : Swift.Codable {
  public var error: Swift.String?
  public var message: Swift.String?
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct ErrorResponse : Swift.Codable {
  public var error: CommonSDK.Common.OldApiError?
  public var newApiError: CommonSDK.Common.Error?
  public init(error: CommonSDK.Common.OldApiError)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension CommonSDK.ValidationType : Swift.Equatable {}
extension CommonSDK.ValidationType : Swift.Hashable {}
extension CommonSDK.ErrorAction : Swift.Equatable {}
extension CommonSDK.ErrorAction : Swift.Hashable {}
extension CommonSDK.ErrorAction : Swift.RawRepresentable {}
extension CommonSDK.Common.Environment : Swift.Equatable {}
extension CommonSDK.Common.Environment : Swift.Hashable {}
extension CommonSDK.Common.Environment : Swift.RawRepresentable {}
extension CommonSDK.Metadata.Method : Swift.Equatable {}
extension CommonSDK.Metadata.Method : Swift.Hashable {}
extension CommonSDK.Metadata.Method : Swift.RawRepresentable {}
